import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ContactPage } from '../pages/contact/contact'
import { ContactsMovilPage } from '../pages/contacts-movil/contacts-movil'

import { listaService } from '../services/service'

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { Contacts } from '@ionic-native/contacts';
import { ResumenCumplePage } from '../pages/resumen-cumple/resumen-cumple';


export const firebaseConfig = {
  apiKey: "AIzaSyDT6BWcsYyu0DdYQS5y0vzZB_3xZEs3YLk",
  authDomain: "cumples-eec97.firebaseapp.com",
  databaseURL: "https://cumples-eec97.firebaseio.com", 
  projectId: "cumples-eec97",
  storageBucket: "cumples-eec97.appspot.com",
  messagingSenderId: "971426279662"
};


@NgModule({
  declarations: [
    MyApp,
    HomePage, ContactPage, ContactsMovilPage,
    ResumenCumplePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,   
    AngularFireAuthModule 
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage, ContactPage, ContactsMovilPage,ResumenCumplePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    listaService, 
    Contacts 
  ]
})
export class AppModule {}
