
import{ Injectable } from '@angular/core'


import { AngularFireDatabase } from '@angular/fire/database'; 
import { debugOutputAstAsTypeScript } from '@angular/compiler';

@Injectable()
export class listaService{

    constructor(public afDB: AngularFireDatabase){}
    
    public getItems(){
        return this.afDB.list("lista/").valueChanges();
    }

    public getItem(id){
        console.log("Imprimiendo getItem: " + this.afDB.list('lista/'+id).valueChanges() ) ;
        return this.afDB.object('lista/'+id).valueChanges();  
    }    

    public editItem(item) {
 
        this.afDB.database.ref('lista/'+item.id).set(item);

    }

    public deleteItem(id) {
        console.log("Impirmiendo lo que se borra .." + id); 
        this.afDB.database.ref('lista/'+ id).remove();  
    }
            
        
    public setItem(item){ 
        console.log("Impirmiendo lo que se modifica.." + item);
        this.afDB.database.ref('lista/'+item.id).set(item);
    }


        
        

    
}


