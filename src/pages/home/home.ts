import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ContactPage } from '../contact/contact'

import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs/Observable';

import { listaService } from '../../services/service'
import { ResumenCumplePage } from '../resumen-cumple/resumen-cumple';
 

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  /*items: Observable<any[]>;

  constructor(public navCtrl: NavController, afDB: AngularFireDatabase    ) {
    this.items = afDB.list('personas').valueChanges();
    console.log(this.items);
  }*/


  Hoy: Observable<any[]>;
  semana: Observable<any[]>;
  mes: Observable<any[]>;
  Hombrete: Observable<any[]>;
  Mujerte: Observable<any[]>;


  items: Observable<any[]>;
  id = null;
  constructor(
    public navCtrl: NavController, 
    private listaServicice: listaService,
    private nav: NavController,
    private navParams: NavParams) {
      this.id = navParams.get('id');
      console.log(this.id);
      this.items = listaServicice.getItems();   
      console.log(this.items);
  }

  goVerCumple(id){
    console.log("Imprimiendo id : " + id);

    this.nav.push(ContactPage, {id: id})
  }

  public goAgregarCumple():void{

    this.navCtrl.push(ContactPage, {id: 0});
  }


  goResumen(){
     this.navCtrl.push(ResumenCumplePage);
  }

  public getSexo(sexo){
    alert('wertyguhj');
    return '../../assets/imgs/logo.png';
  }



}  