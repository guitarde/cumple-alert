import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { listaService } from '../../services/service';
 
import { Contacts, ContactFieldType, ContactFindOptions, ContactName, ContactField} from '@ionic-native/contacts';


 @Component({
  selector: 'page-contacts-movil',
  templateUrl: 'contacts-movil.html',
})
export class ContactsMovilPage {

  contacto:any;
  
  constructor(
      public navCtrl: NavController, 
      public navParams: NavParams, 
      private contact: Contacts,
      private listaServicio: listaService
    ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactsMovilPage');
  }

 public async getContacts(){ 
    try {
      const selectedContact = await this.contact.pickContact();
      alert ("selected : "+ selectedContact.displayName);
      console.log(JSON.stringify({selectedContact}));
      //this.contacto = JSON.stringify({selectedContact});
      this.navCtrl.pop();
      return JSON.stringify({selectedContact});
    } catch (e) {
      alert("Error getContacts:" + e);
    }

  }

  async addContact(){
    try {
      const newContact = this.contact.create();
      newContact.name = new ContactName(null, "Luis reyna", "holida");
      newContact.phoneNumbers = [new ContactField('mobile', '08767678988')];
      newContact.save();
      alert("Añadido correctamente")
    } catch (e) {
      alert("Error create :" + e);
    }
  }

  async findContact(){
    try {
      const options = new ContactFindOptions();

      options.filter = "A test Contact";
      options.hasPhoneNumber = true;

      const fields : ContactFieldType[] = ['name'];
      const filteredContacts = await this.contact.find(fields, options);
      alert("Filtered contatcs : " + filteredContacts);
    } catch (e) {
      alert("Eerror find " + e);
    }
  }

}
