import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewCumplePage } from './view-cumple';

@NgModule({
  declarations: [
    ViewCumplePage,
  ],
  imports: [
    IonicPageModule.forChild(ViewCumplePage),
  ],
})
export class ViewCumplePageModule {}
