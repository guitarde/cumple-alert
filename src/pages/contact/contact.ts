import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { listaService } from '../../services/service';
import { ContactsMovilPage } from '../contacts-movil/contacts-movil';
import { Contacts } from '@ionic-native/contacts';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  item = null;
  //id = {id: "", email: ""};
  id = null;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private  listaService: listaService ,
    private contact: Contacts,

  ) {
      this.id = navParams.get('id');

      console.log("ID desde parametros crear cumple" + this.id);
      if (this.id != 0) {
          this.item = listaService.getItem(this.id)
          .subscribe(item => {
            this.item = item; 
          }); 
          console.log("Constructor crear cumple" + this.item);
      } else {
        this.item ={nombre :"", id:""};
      }
  } 

  public guardarCumple(){

    if (this.id != 0){
      this.listaService.editItem(this.item);
      alert("Cumple editado con éxito");  
    } else {
      if (this.item.sexo != 'hombrete' || this.item.sexo != 'mujerte')
        this.item.sexo = 'vacio'; 
      this.item.id = Date.now();
      this.listaService.setItem(this.item);
      alert("Cumple creado con éxito");
    }
    this.navCtrl.pop();
  }

  public eliminarCumple(){
    this.listaService.deleteItem(this.item);
    alert("Cumple borrado con éxito");
    this.navCtrl.pop();

  }

  public goContacts(){
    
    this.navCtrl.push(ContactsMovilPage);
  }
  
  public async getContacts(){ 
    try {
      const selectedContact = await this.contact.pickContact();
      //alert ("selected : "+ selectedContact.displayName);
 
      this.setDataContact(selectedContact);
    } catch (e) { 
      alert("Error getContacts:" + e);
    }
  }

  public setDataContact(datos){
    this.item.nombre =  datos.displayName;
    this.item.email = "";
    this.item.telefono = ""; 

    try {    
      JSON.stringify(datos.phoneNumbers
        .map(numero => {
          this.item.telefono += numero.value+ ";";
      }));
    } catch (e) { 
      //alert("Error al leer contactos:" + e);
    }

    try {
      JSON.stringify(datos.emails 
        .map(email => {
          this.item.email += email.value +" ";
      }));
    } catch (e) { 
      //alert("Error al leer los emails:" + e);
    }
  }

}
/*
{"selectedContact":{"_objectInstance":{"id":"3","rawId":"2","displayName":"holida Luis
[app-scripts]             reyna","name":{"familyName":"Luis reyna","givenName":"holida","formatted":"holidaLuis
[app-scripts]             reyna"},"nickname":null,"phoneNumbers":[{"id":"4","pref":false,"value":"08767678988","type":"mobile"}],"emails":null,"addresses":null,"ims":null,"organizations":null,"birthday":null,"note":null,"photos":null,"categories":null,"urls":null}}}
*/